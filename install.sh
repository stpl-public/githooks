#!/bin/bash
hpath=~/.githooks
echo $hpath
if [ -d "$hpath" ]; then
   echo "Previous installation found... updating"
   cd $hpath && git add . && git stash && git checkout master && git pull
else
   echo "Doing fresh installation..."
   cd ~/ && git clone https://gitlab.com/stpl-public/githooks.git .githooks
fi
git config --global core.hooksPath $hpath
echo Done!
